package com.example.a17pulltorequest

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.cardview.view.*

class Adapter(private val context: Context, private val list: List<Data>) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, type: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview, viewGroup, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.myImage.setImageResource(list[position].cardImage)
        holder.myName.text = list[position].cardName
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val myImage = itemView.imageView
        val myName = itemView.textView
    }
}